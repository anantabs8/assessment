package com.dkatalis.jfs.workflow;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class SpringBootProblem15 {
    public static void main(String[] args) {
        try {
            Scanner scan = new Scanner(System.in);
            System.out.print("input n prima: ");
            Integer n = scan.nextInt();
            System.out.println(n);
            calculatePrima(n);
        } catch (Exception e) {
            System.out.println("error input must be numeric reload your machine");
        }
        
    }
    public static void calculatePrima(Integer n) {
        int i = 2;
        List<Integer> temp = new ArrayList<>();
        while(temp.size() != n) {
            if(i % 2 == 0 || i % 3 == 0 || i % 5 == 0) {
                if(i == 2) {
                    temp.add(i);
                }else if(i == 3) {
                    temp.add(i);
                }else if(i == 5) {
                    temp.add(i);
                }else if(i == 7) {
                    temp.add(i);
                }
            }else {
                temp.add(i);
            }
            i++;
        }
        System.out.println(temp.toString());
    }
}
